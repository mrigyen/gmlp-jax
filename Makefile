test:
	python3 gmlp.py
	python3 gmlpstack.py
	python3 amlp.py
	python3 amlpstack.py
	python3 dataloader.py
	python3 get_model_size.py --seq_len 64 --stack_len 3 --hidden_dim 32 --architecture "gMLP"
	python3 jax_tokenizer.py
	python3 np_tokenizer.py
	python3 sampling.py
	python3 train.py --root_dir "dataset/" --save_path "checkpoints/a.pkl" --stack_len 3 --seq_len 9 --hidden_dim 4 --sub_batch_size 1 --architecture aMLP
	python3 generate.py --model_path "checkpoints/a.pkl.step_TRAINING_COMPLETE" --architecture "aMLP"
