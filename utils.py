import jax
import jaxlib
import jax.numpy as jnp
import functools
import pickle
from jax.experimental import optimizers


@jax.jit
def nat_log_to_binary_log(nat_log: jaxlib.xla_extension.DeviceArray):
    return nat_log / jnp.log(2)


@jax.jit
def perplexity(cross_entropy: jaxlib.xla_extension.DeviceArray):
    return 2 ** nat_log_to_binary_log(cross_entropy)


@functools.partial(jax.pmap, axis_name="devices")
def psum(x):
    return jax.lax.psum(x, "devices")


def sync_training_end(ended: bool):
    x = jnp.array([1]) if ended else jnp.array([0])
    xr = jax.device_put_replicated(x, jax.local_devices())
    return jnp.sum(psum(xr)) > 0


def save_model(
    save_path,
    step,
    replicated_params,
    d_Z,
    seq_len,
    stack_len,
    replicated_opt_state,
    architecture,
):
    # save a checkpoint
    with open(save_path + ".step_" + str(step), "wb") as file:
        params = jax.device_get(jax.tree_map(lambda x: x[0], replicated_params))
        opt_state = jax.device_get(jax.tree_map(lambda x: x[0], replicated_opt_state))
        pickle.dump(
            {
                "params": params,
                "d_Z": d_Z,
                "seq_len": seq_len,
                "stack_len": stack_len,
                "opt_state": optimizers.unpack_optimizer_state(opt_state),
                "architecture": architecture,
            },
            file,
        )
