import amlp
import jax
from jax import random
import jax_tokenizer as tokenizer
import functools


def AMLPStack(seq_len: int, d_Z: int, stack_len: int = 3):
    if d_Z % 2 != 0:
        raise Exception("d_Z value should be even otherwise SGU splitting cannot occur")
    if d_Z == 0:
        raise Exception("d_Z value should greater than 0")

    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        list_params = []
        list_applys = []
        for i in range(stack_len):
            init_model, apply_model = amlp.AMLPBlock(seq_len, d_Z)
            key, rng = random.split(rng)
            input_shape, params, applys = init_model(key, input_shape)
            list_params.append(params)
            list_applys.append((applys, apply_model))
        params = {"amlps": list_params}
        return input_shape, params, tuple(list_applys)

    @functools.partial(jax.jit, static_argnums=(1,))
    def apply_fun(params, list_applys, x):
        for i in range(stack_len):
            applys, apply_model = list_applys[i]
            x = apply_model(params["amlps"][i], applys, x)
        return x

    return init_fun, apply_fun


if __name__ == "__main__":
    rng = random.PRNGKey(0)
    seq_len = 64
    d_Z = 768
    x = tokenizer.pad_right(tokenizer.tokenize("Hello world!"), seq_len)
    print(x.shape)
    init_s, apply_s = AMLPStack(seq_len, d_Z)

    key, rng = random.split(rng)
    output_shape, list_params, list_applys = init_s(key, (seq_len, 256))
    out = apply_s(list_params, list_applys, tokenizer.magnify(x))
    print(out.shape)
    real_out = tokenizer.untokenize(tokenizer.reduce(out))
    print(real_out)
