# load jsonl language model datasets for training
# target dataset is The Pile
# now with parallel processing and pre-fetching
#
# extremely thankful to the author of https://teddykoker.com/2020/12/dataloader/
# for writing his guide - it really helped me understand the subtleties of
# implementing this

import jsonlines
import os
import np_tokenizer as tokenizer
import numpy as np
from numba import jit

exception_nojsonl = Exception("Root dir contains no jsonl files")


class DataLoader:
    def __init__(self, root_dir, num_devices, sub_batch_size, seq_len):
        self.root_dir = root_dir
        self.num_devices = num_devices
        self.sub_batch_size = sub_batch_size
        self.seq_len = seq_len
        batch_size = self.sub_batch_size * self.num_devices
        # min since tokenization might mean one char can consist of multiple
        # tokens
        self.size_per_batch = self.seq_len + batch_size

        # make a list of jsonl files whose lines we iterate over
        self.index_paths = []
        for file in os.listdir(self.root_dir):
            if file.endswith(".jsonl"):
                self.index_paths.append(os.path.join(self.root_dir, file))
        if len(self.index_paths) == 0:
            raise exception_nojsonl

    def iterate(self):
        text = ""
        batch_size = self.sub_batch_size * self.num_devices
        # min since tokenization might mean one char can consist of multiple
        # tokens
        min_str_size_per_batch = self.seq_len + batch_size

        for fp in (jsonlines.open(file) for file in self.index_paths):
            while True:
                try:
                    if len(text) >= min_str_size_per_batch:
                        yield text[:min_str_size_per_batch]
                        # a batch contains `batch_size` num of
                        # prompt-continuation pairs
                        text = text[batch_size:]
                    else:
                        sample = fp.read()["text"]
                        # end-of-text token is \0
                        text = text + "\0" + sample
                except EOFError:
                    fp.close()
                    break

    def process(self, text: str):
        tokenized_text = tokenizer.tokenize(text)
        return numba_process(
            tokenized_text,
            self.size_per_batch,
            self.seq_len,
            self.sub_batch_size,
        )

    def __iter__(self):
        for text in self.iterate():
            yield self.process(text)


@jit(nopython=True)
def numba_process(tokenized_text, size_per_batch, seq_len, sub_batch_size):
    # clip the tokenized_text to ensure all prompt and continuation values
    # are predictable
    tokenized_text = tokenized_text[:size_per_batch]
    batch_prompts_created = False
    prompts_created = False
    # for "123456", text[i:i+seq_len] gets ["123", "234", "345"]
    for i in range(len(tokenized_text)):
        prompt = tokenized_text[i : i + seq_len]
        continuation = tokenized_text[i + 1 : i + seq_len + 1]
        if len(prompt) == seq_len and len(continuation) == seq_len:
            if not prompts_created:
                prompts = np.expand_dims(prompt, axis=0)
                continuations = np.expand_dims(continuation, axis=0)
                prompts_created = True
            else:
                prompts = np.concatenate((prompts, np.expand_dims(prompt, axis=0)))
                continuations = np.concatenate(
                    (continuations, np.expand_dims(continuation, axis=0))
                )
            if len(prompts) == sub_batch_size:
                if not batch_prompts_created:
                    batch_prompts = np.expand_dims(prompts, axis=0)
                    batch_continuations = np.expand_dims(continuations, axis=0)
                    batch_prompts_created = True
                else:
                    batch_prompts = np.concatenate(
                        (batch_prompts, np.expand_dims(prompts, axis=0))
                    )
                    batch_continuations = np.concatenate(
                        (batch_continuations, np.expand_dims(continuations, axis=0))
                    )
                prompts_created = False
    return batch_prompts, batch_continuations


if __name__ == "__main__":
    # test
    root_dir = "/mnt/WD_2TB/datasets/pilex/tiny"
    num_devices = 1
    sub_batch_size = 3
    seq_len = 2
    dataloader = DataLoader(root_dir, num_devices, sub_batch_size, seq_len)
    for i, (prompt, continuation) in enumerate(dataloader):
        print(i, prompt.shape, continuation.shape)
    for i, (prompt, continuation) in enumerate(dataloader):
        print(i, prompt.shape, continuation.shape)
