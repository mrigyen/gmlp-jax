import jax
import jax.numpy as jnp
import jaxlib
import log


@log.out
@jax.jit
def sample(
    key,
    logits: jaxlib.xla_extension.DeviceArray,
    top_k,
    top_p,
    temp,
) -> int:
    top_k_sampled_logits = top_k_sampling(logits, top_k)
    top_p_sampled_logits = top_p_sampling(top_k_sampled_logits, top_p)
    softmax_sampled_logit = softmax_sampling(key, top_p_sampled_logits, temp=temp)
    return softmax_sampled_logit


@log.out
@jax.jit
def softmax_sampling(key, logits, temp=1):
    # jax.random.categorical takes unnormalized log probabilities
    return jax.random.categorical(key, logits / temp, axis=-1)


@log.out
@jax.jit
def top_k_sampling(logits, top_k):
    assert len(logits.shape) == 1, "fx written assuming we only send one token"
    # keep in mind the array is flattened before sorting if you don't specify
    # an axis
    # TODO check whether there are any issues with this code
    sorted_indices = jnp.argsort(logits)[::-1]
    indices_range = jnp.arange(len(sorted_indices))
    # correct for zero indexing using >= instead of >
    sorted_indices_to_remove = indices_range >= top_k
    _, indices_to_remove = jax.lax.sort_key_val(
        sorted_indices, sorted_indices_to_remove
    )
    logit_mask = 1e10 * indices_to_remove
    return logits - logit_mask


@log.out
@jax.jit
def top_p_sampling(logits, top_p):
    assert len(logits.shape) == 1, "fx written assuming we only send one token"
    sorted_logits = jnp.sort(logits)[::-1]
    sorted_indices = jnp.argsort(logits)[::-1]
    cumulative_probs = jnp.cumsum(jax.nn.softmax(sorted_logits))
    # Remove tokens with cumulative probability above a threshold
    sorted_indices_to_remove = cumulative_probs > top_p
    # manually ensure that if cumulative_probs[0] > top_p, there still is one
    # logit that is left untouched
    sorted_indices_to_remove = sorted_indices_to_remove.at[0].set(False)
    _, indices_to_remove = jax.lax.sort_key_val(
        sorted_indices, sorted_indices_to_remove
    )
    logit_mask = 1e10 * indices_to_remove
    return logits - logit_mask


if __name__ == "__main__":
    rng = jax.random.PRNGKey(0)
    key, rng = jax.random.split(rng)
    c = jnp.array([-2344, 1, 2, 3, 3, 2, 1])
    top_k = 10
    top_p = 0.95
    temp = 1
    print(top_k_sampling(c, top_k))
    print(top_p_sampling(c, top_p))
    print(sample(key, c, top_k, top_p, temp))
