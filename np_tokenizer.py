import numpy as np
from numba import jit

EMBEDDING_LEN = 256


def tokenize(seq: str):
    # return minified tokenized representations of text
    return np.array([c for c in seq.encode("utf-8")])


@jit(nopython=True)
def one_hot_encoding(x: int):
    return np.where(np.arange(EMBEDDING_LEN) == x, 1, 0)


if __name__ == "__main__":
    print(one_hot_encoding(1))
