import argparse
import functools
import amlpstack
import gmlpstack
from jax import random
import jax_tokenizer as tokenizer
import jax
import jax.numpy as jnp
import jaxlib
import log
import generate
import time
from jax.experimental import optimizers
from dataloader import DataLoader
import utils
import pickle


parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "--root_dir",
    dest="root_dir",
    metavar="path",
    type=str,
    required=True,
    help="path to directory containing jsonl files used for training",
)
parser.add_argument(
    "--seq_len", dest="seq_len", type=int, help="length of input sequence"
)
parser.add_argument(
    "--stack_len",
    dest="stack_len",
    type=int,
    help="number of gmlp /amlp blocks you want stacked in the architecture",
)
parser.add_argument(
    "--hidden_dim",
    dest="d_Z",
    type=int,
    help="out dimension of first projection in a gMLP / aMLP block",
)
parser.add_argument(
    "--sub_batch_size",
    dest="sub_batch_size",
    type=int,
    help="number of samples per device per step",
)
parser.add_argument(
    "--learning_rate",
    dest="learning_rate",
    type=float,
    default=1e-3,
    help="learning rate",
)
parser.add_argument(
    "--save_path",
    dest="save_path",
    metavar="path",
    type=str,
    required=False,
    default="mlpstack.pkl",
    help="path to save the params as a pickle file",
)
parser.add_argument(
    "--load_path",
    dest="load_path",
    metavar="path",
    type=str,
    required=False,
    help="path to load the params as a pickle file before resuming training",
)
# using integers because the parsing of False (shell string argument) as True (python object) occurs
parser.add_argument(
    "--architecture",
    dest="architecture",
    type=str,
    required=True,
    help="choose between the use of gMLP and aMLP blocks",
)
parser.add_argument("--seed", dest="seed", type=int, default=0, help="PRNG seed")
parser.add_argument(
    "--start_mini_batch_index",
    dest="start_mini_batch_index",
    type=int,
    default=0,
    help="The mini-batch to resume training on",
)


@functools.partial(jax.vmap, in_axes=(None, None, None, 0, 0))
@functools.partial(jax.jit, static_argnums=(1, 2))
def softmax_cross_entropy(
    params: jaxlib.xla_extension.DeviceArray,
    apply_model,
    applys,
    prompt: jaxlib.xla_extension.DeviceArray,
    continuation: jaxlib.xla_extension.DeviceArray,
):
    """
    params: the parameters of the network, over which you do a gradient descent
    apply_model: the actual forward
    applys: internal forwards
    prompt: (seq_len, encoding_len) input to the model
    continuation: (seq_len, encoding_len) ideal output of the model
    """
    model_logits = apply_model(params, applys, prompt)

    # return the cross entropy of the generated sequence
    return -jnp.sum(continuation * jax.nn.log_softmax(model_logits))


@functools.partial(jax.jit, static_argnums=(1, 2))
def batch_softmax_cross_entropy(params, apply_model, applys, prompts, continuations):
    batch_loss = softmax_cross_entropy(
        params, apply_model, applys, prompts, continuations
    )
    return jnp.average(batch_loss, axis=0)


@jax.jit
@log.out
def spectral_norms(params):
    return [
        jnp.linalg.svd(W, compute_uv=False)[0]
        for W in jax.tree_util.tree_leaves(params)
        if len(W.shape) == 2
    ]


def train(args):
    rng = random.PRNGKey(args.seed)
    init_fun_adam, update_fun_adam, get_params_adam = optimizers.adam(
        args.learning_rate
    )
    if args.load_path:
        with open(args.load_path, "rb") as file:
            model = pickle.load(file)
        seq_len = model["seq_len"]
        if args.architecture == "aMLP":
            init_model, apply_model = amlpstack.AMLPStack(
                model["seq_len"],
                model["d_Z"],
                model["stack_len"],
            )
        elif args.architecture == "gMLP":
            init_model, apply_model = gmlpstack.GMLPStack(
                model["seq_len"],
                model["d_Z"],
                model["stack_len"],
            )
        else:
            raise Exception("unknown architecture")

        key, rng = random.split(rng)
        _, _, applys = init_model(key, (model["seq_len"], 256))
        params = model["params"]
        opt_state = optimizers.pack_optimizer_state(model["opt_state"])
    else:
        seq_len = args.seq_len
        if args.architecture == "aMLP":
            init_model, apply_model = amlpstack.AMLPStack(
                args.seq_len, args.d_Z, args.stack_len
            )
        elif args.architecture == "gMLP":
            init_model, apply_model = gmlpstack.GMLPStack(
                args.seq_len, args.d_Z, args.stack_len
            )
        else:
            raise Exception("unknown architecture")

        key, rng = random.split(rng)
        output_shape, params, applys = init_model(key, (args.seq_len, 256))
        opt_state = init_fun_adam(params)

    dataloader = DataLoader(
        args.root_dir,
        jax.local_device_count(),
        args.sub_batch_size,
        args.seq_len,
    )

    local_devices = jax.local_devices()
    replicated_opt_state = jax.device_put_replicated(opt_state, local_devices)
    replicated_params = jax.device_put_replicated(params, local_devices)

    @functools.partial(jax.pmap, axis_name="data", in_axes=(None, 0, 0, 0, 0))
    def update(step, params, opt_state, prompts, continuations):
        loss, grads = jax.value_and_grad(batch_softmax_cross_entropy)(
            params,
            apply_model,
            applys,
            prompts,
            continuations,
        )
        grads = jax.lax.pmean(grads, axis_name="data")
        loss = jax.lax.pmean(loss, axis_name="data")
        opt_state = update_fun_adam(step, grads, opt_state)
        return get_params_adam(opt_state), loss, opt_state

    broken = False
    start = time.perf_counter()
    for i, (prompts, continuations) in enumerate(dataloader):
        if i < args.start_mini_batch_index:
            print(f"Skipped mini-batch of index {i}")
            continue
        if utils.sync_training_end(False):
            broken = True
            break
        prompts = tokenizer.magnify(jnp.array(prompts))
        continuations = tokenizer.magnify(jnp.array(continuations))
        replicated_params, loss, replicated_opt_state = update(
            jnp.array(log.step),
            replicated_params,
            replicated_opt_state,
            prompts,
            continuations,
        )
        # the loss and grads across each device is the same
        loss = loss[0]
        end = time.perf_counter()
        scalars = {
            "cross-entropy-loss": loss,
            "perplexity": utils.perplexity(loss),
            "bits-per-character": utils.nat_log_to_binary_log(loss),
            # "avg_spectral_norm": jnp.average(
            #     spectral_norms(jax.tree_map(lambda x: x[0], replicated_params))
            # ),
            # this is time taken per step, including getting data
            "time": end - start,
        }
        # jnp.average cannot take a tuple as an argument for axis. seems like a bug
        # averaged_activations = jnp.mean(replicated_activations, axis=(0, 2, 3))
        # std_activations = jnp.std(replicated_activations, axis=(0, 2, 3))
        # for layer in range(args.stack_len):
        #     scalars["avg_activation_" + str(layer)] = averaged_activations[layer]
        #     scalars["std_activation_" + str(layer)] = std_activations[layer]

        # save a checkpoint after every 1M steps
        # include initial generation as a baseline
        if i % 1e6 == 0:
            utils.save_model(
                args.save_path,
                i,
                replicated_params,
                args.d_Z,
                args.seq_len,
                args.stack_len,
                replicated_opt_state,
                args.architecture,
            )

            # log a generation
            key, rng = random.split(rng)
            default_prompt = "Let justice be done though the heavens fall"
            default_prompt = default_prompt[: args.seq_len]
            tokenized_and_padded_prompt = tokenizer.pad_right(
                tokenizer.tokenize(default_prompt), args.seq_len
            )
            continuation = generate.generate_until(
                key,
                jax.device_get(jax.tree_map(lambda x: x[0], replicated_params)),
                applys,
                tokenized_and_padded_prompt,
                apply_model,
                seq_len,
            )
            log.info(f"prompt: {default_prompt}")
            continuation = tokenizer.untokenize(continuation)
            log.info(f"continuation: {continuation}")
            scalars["prompt"] = default_prompt
            scalars["continuation"] = continuation

        log.scalars(scalars)

        start = time.perf_counter()

    if not broken:
        utils.sync_training_end(True)

    # log a generation
    key, rng = random.split(rng)
    default_prompt = "Let justice be done though the heavens fall"
    default_prompt = default_prompt[: args.seq_len]
    tokenized_and_padded_prompt = tokenizer.pad_right(
        tokenizer.tokenize(default_prompt), args.seq_len
    )
    continuation = generate.generate_until(
        key,
        jax.device_get(jax.tree_map(lambda x: x[0], replicated_params)),
        applys,
        tokenized_and_padded_prompt,
        apply_model,
        seq_len,
    )
    log.info(f"prompt: {default_prompt}")
    continuation = tokenizer.untokenize(continuation)
    log.info(f"continuation: {continuation}")

    # save a copy of the model after completing training
    utils.save_model(
        args.save_path,
        "TRAINING_COMPLETE",
        replicated_params,
        args.d_Z,
        args.seq_len,
        args.stack_len,
        replicated_opt_state,
        args.architecture,
    )


if __name__ == "__main__":
    args = parser.parse_args()
    train(args)
