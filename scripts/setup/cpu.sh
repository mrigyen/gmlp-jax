virtualenv .
source env.sh
pip install --upgrade pip
pip install "jax[cpu]"
pip install jsonlines
pip install numba
pip install wandb
