#!/usr/bin/env bash

DEVICE_ID=$(echo $1 + 1 | bc)
TOTAL_DEVICES=$2
sudo apt update
sudo apt install zstd
wget https://mystic.the-eye.eu/public/AI/pile/val.jsonl.zst
unzstd val.jsonl.zst
mkdir -p dataset/
split -n l/$DEVICE_ID/$TOTAL_DEVICES val.jsonl > dataset/val.jsonl
wc -l dataset/val.jsonl
