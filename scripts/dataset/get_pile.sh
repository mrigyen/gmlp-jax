#!/usr/bin/env bash

DEVICE_ID=$(echo $1 + 1 | bc)
TOTAL_DEVICES=$2
PILE_ID=$3
sudo apt update
sudo apt install zstd
if PILE_ID
wget https://mystic.the-eye.eu/public/AI/pile/train/$PILE_ID.jsonl.zst
unzstd $PILE_ID.jsonl.zst
mkdir -p dataset/
split -n l/$DEVICE_ID/$TOTAL_DEVICES $PILE_ID.jsonl > dataset/$PILE_ID.jsonl
wc -l dataset/$PILE_ID.jsonl
