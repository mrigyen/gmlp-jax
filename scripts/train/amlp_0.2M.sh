#!/usr/bin/env bash
# % python3 get_model_size.py --seq_len 64 --stack_len 3 --hidden_dim 32
#   --architecture "aMLP"
# 0.20136M
STACK_LEN=3
HIDDEN_DIM=32
SEQ_LEN=64
BATCH_SIZE=128
MODEL_NAME="a0.2M"
DATASET_DIR="$HOME/dataset/"
ARCHITECTURE="aMLP"
SEED=0
mkdir -p checkpoints/

if $(sudo lsof -w /dev/accel0); then
	echo "TPU device already in use by a process"
else
	# train the model
	python3 train.py \
		--root_dir "$DATASET_DIR" \
		--seq_len $SEQ_LEN \
		--stack_len $STACK_LEN \
		--hidden_dim $HIDDEN_DIM \
		--sub_batch_size $BATCH_SIZE \
		--save_path checkpoints/$MODEL_NAME.pkl \
		--architecture $ARCHITECTURE \
		--seed $SEED
fi
