JAX codebase for training MLP-based language models. Intended to be used
to evaluate meta-learning capabilities of MLP-based language models (and
by extension, MLP architectures). Currently only contains architecture
for gMLPs and aMLPs (see [{2105.08050} Pay Attention to
MLPs](https://arxiv.org/abs/2105.08050)). Supports TPU device VMs and
TPU pod VMs for training at scale using data parallelism.

Intended to be used to train MLP-based language models on The Pile
dataset.

Intentionally (and unintentionally) suckless codebase choices:

-   uses vanilla JAX (no `flax`{.verbatim}, no `haiku`{.verbatim})
-   does not use any form of multiprocessing or threading for the
    dataloader
-   dataloader is written from scratch to support only The Pile dataset,
    and does not use or import PyTorch or Tensorflow for dataloading
-   does not use `optax`{.verbatim} for optimizers, uses vanilla JAX
    optimizers
-   does not use MLOps solutions for visual logging such as
    `wandb`{.verbatim}, `tensorboard`{.verbatim}, or
    `guildai`{.verbatim}. Exhaustive logging is done to a file though,
    which can be used to visualize the training after-the-fact, or to
    observe the status of the training via a
    `tail -f $LOGFILE`{.verbatim}

Next:

-   Evaluate meta-learning for gMLP-small and aMLP-small on 40GB data
-   Create a new / modified architecture based on existing literature
    and evaluate its performance compared to the previous two models. I
    might call the most viable one xMLP.
-   Train gMLP-small, aMLP-small, and xMLP-small on bigger and bigger
    capacity datasets to see how it affects meta-learning
-   Train gMLP-medium, aMLP-medium, and xMLP-medium on 40GB data and
    evaluate meta-learning capabilities, especially in comparison to
    previously created models.
-   Continue until you have enough empirical evidence to claim something
    concrete about meta-learning and MLPs.
